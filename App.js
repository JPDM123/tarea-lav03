
import React from 'react';
import { Component } from "react";
import Message from './app/components/Message/Message';
import Body from './app/components/Vody/Vody'
import MyList from './app/components/Vody/myList'
import AgeValidator from './app/components/Vody/AgeValidator'
import {StyleSheet, TouchableOpacity, TextInput, Text, View, Image,FlatList,SafeAreaView} from 'react-native';

export default class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
    };
  
  }

  changeTextInput = text => {
    console.log(text)
    this.setState({textValue: text});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text> Ingrese su edad </Text>
        </View>
        
        <TextInput 
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text =>this.changeTextInput(text)}
          type="numeric"
          keyboardType='numeric'
          placeholder='Numeric'
          value={this.state.textValue}
        />


        <AgeValidator valor={this.state.textValue}/>
        <MyList/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button :{
    top:10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding : 10,
  },
  countContainer : {
    alignItems : 'center',
    padding : 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  countText : {
    alignItems : 'center',
    color: '#FF00FF',
  },
  name: {
    fontSize: 32,
    color: '#FF00FF',
  },
});

